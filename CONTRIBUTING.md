A short contribution "guide":

I started this project as i missed a display on the scooter and i'm not keen about pulling out my phone to start a app and connect it to see some values or to mount my phone to the handlebar of the scooter.

My intention changed over time, and also the environment of the M365 "hackers" changed. 
Tthere's people stealing open source code, not publishing their changed versions (going closed source) and there's people selling displays based on this project.

As i'm not doing more free work for them, this repo is the part of the initial project that i'll update from time to time and also share.

So this repo is not stale, but i'm putting my efforts into another repo with some more functionality which i'm not gonna share public.
But as the other projects share the same code basis with this project and i still use the display-part, i'll also update this project.

If you want to build a display on my code basis go ahead.
If you want to sell a display - there's a license.... please think about it.
If you want to make your own fork - there's a license, please respect it and share your code as well.

And if you want to contribute to this project - get in touch (see link to telegram group in readme) and let me know which function/features you have in mind.
Then make a fork, implement your stuff, make a PR... at that moment, you'll have taken your time to actually deal with the code and know about some more details - and you showed your dedication to OS.
So at that moment we can also talk about giving you access to the private repo with some more functionality.

And your code can be merged to the public repo/stay public or it can be moved to the private repo - as you like.

I'm sorry if you find this a bit complicated, but i think it's the only way to find out if someone is interested in contributing or just wants to copy your code,
or even ("Hello Arkaitxu!, yes, i mean you!) keen to ask for features to be implemented so he can use it to sell his rippoff displays!
